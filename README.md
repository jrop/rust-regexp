# rust-regexp

[![pipeline status](https://gitlab.com/jrop/rust-regexp/badges/master/pipeline.svg)](https://gitlab.com/jrop/rust-regexp/commits/master)

The purpose of this repository is to learn how Regular Expression recognizers are implemented.
