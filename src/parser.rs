mod ast;

#[derive(Debug)]
pub enum ReToken {
  Literal(char),
  Caret,
  Dot,
  Plus,
  Star,
  Question,
  Or,
  LBracket,
  RBracket,
  LParen,
  RParen,
  LBrace,
  RBrace,
}

pub struct ReLexer<'a> {
  chars: std::iter::Enumerate<std::str::Chars<'a>>,
}
impl<'a> ReLexer<'a> {
  pub fn new(source: &str) -> ReLexer {
    ReLexer {
      chars: source.chars().enumerate(),
    }
  }

  pub fn peek(&mut self) -> Option<(usize, ReToken)> {
    let mut chars = self.chars.clone();
    match chars.next() {
      Some((i, c)) => match c {
        '\\' => match chars.next() {
          Some((i, n)) => Some((i, ReToken::Literal(n))),
          None => Some((i, ReToken::Literal('\\'))),
        },
        '^' => Some((i, ReToken::Caret)),
        '.' => Some((i, ReToken::Dot)),
        '*' => Some((i, ReToken::Star)),
        '+' => Some((i, ReToken::Plus)),
        '?' => Some((i, ReToken::Question)),
        '|' => Some((i, ReToken::Or)),
        '[' => Some((i, ReToken::LBracket)),
        ']' => Some((i, ReToken::RBracket)),
        '(' => Some((i, ReToken::LParen)),
        ')' => Some((i, ReToken::RParen)),
        '{' => Some((i, ReToken::LBrace)),
        '}' => Some((i, ReToken::RBrace)),
        _ => Some((i, ReToken::Literal(c))),
      },
      None => None,
    }
  }

  pub fn next(&mut self) -> Option<ReToken> {
    let peeked = self.peek();
    match peeked {
      Some((peeked_i, t)) => {
        while let Some((next_i, _)) = self.chars.next() {
          if next_i == peeked_i {
            break;
          }
        }
        Some(t)
      }
      None => None,
    }
  }
}

pub struct ReParser<'a> {
  lexer: ReLexer<'a>,
}
impl<'a> ReParser<'a> {
  pub fn bp(&self) {}
  pub fn nud(&self, t: ReToken) -> ast::ReNode {
    match t {
      ReToken::Caret => {
        ast::ReNode::Sequence(Box::new(ast::ReNode::Caret), Box::new(self.parse("TODO")))
      }
      ReToken::Literal(c) => ast::ReNode::Sequence(
        Box::new(ast::ReNode::Literal(c)),
        Box::new(self.parse("TODO")),
      ),
      _ => ast::ReNode::TODO,
    }
  }
  pub fn led(&self) -> ast::ReNode {
    ast::ReNode::TODO
  }
  pub fn parse(&self, s: &str) -> ast::ReNode {
    ast::ReNode::TODO
  }
}
