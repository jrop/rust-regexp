pub enum ReNode {
  Literal(char),
  Caret,
  Dot,
  Plus,
  Star,
  Question,
  Or,
  Sequence(Box<ReNode>, Box<ReNode>),
  TODO,
}
